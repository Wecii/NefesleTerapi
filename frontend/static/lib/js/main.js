jQuery(document).ready(function () {
    (function () {
        var w = 1072;
        var h = 100;
        var sw = jQuery('.static-header').width();
        var th = sw * h / w;
        jQuery('.static-header').css('height', th + 'px');
        jQuery(window).resize(function () {
            var w = 1072;
            var h = 100;
            var sw = jQuery('.static-header').width();
            var th = sw * h / w;
            jQuery('.static-header').css('height', th + 'px');
        });
    })();
});

(function () {
    var wa = document.createElement('link');
    wa.rel = "stylesheet";
    wa.async = true;
    wa.href = '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wa, s);
})();